import React from 'react';
import News from './containers/News';
import './style/main.scss';

const App = () => (
  <div className="App">
    <News />
  </div>
);

export default App;
