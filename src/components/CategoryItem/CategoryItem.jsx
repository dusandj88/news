import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Slider from 'react-slick';
import PropTypes from 'prop-types';
import { fetchCategoryNews } from 'API';
import SliderItem from 'components/SliderItem/SliderItem';
import newsActions from 'redux/news/actions';
import navigationActions from 'redux/navigation/actions';
import uuid from 'react-uuid';

const settings = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 1,
  initialSlide: 0,
  fade: true,
  arrows: true,
  className: 'category-item-slider',
};

const CategoryItem = ({ category }) => {
  const [articles, setArticles] = useState([]);
  const [expanded, setExpanded] = useState(true);

  const reduxCountry = useSelector((state) => state.country);

  const dispatch = useDispatch();

  useEffect(() => {
    let mounted = true;
    async function fetchData() {
      const data = await fetchCategoryNews(reduxCountry.country, category);
      if (data !== []) {
        if (mounted) {
          setArticles(data.articles);
        }
      }
    }
    fetchData();

    return () => (mounted = false);
  }, [reduxCountry.country, category]);

  return (
    <div className="category-item__container">
      <div className="category-item-title__container">
        <div
          className="category-item__title"
          role="button"
          tabIndex="0"
          onClick={() => {
            dispatch(newsActions.setActiveCategory(category));
            dispatch(navigationActions.setActiveNavigationTab(0));
          }}
          onKeyPress={() => {
            dispatch(newsActions.setActiveCategory(category));
            dispatch(navigationActions.setActiveNavigationTab(0));
          }}
        >
          {category}
        </div>
        <span
          onClick={() => setExpanded(!expanded)}
          onKeyPress={() => setExpanded(!expanded)}
          role="button"
          tabIndex="0"
        >
          expand/collapse
        </span>
      </div>
      {expanded ? (
        <div className="category-items__container">
          {articles !== undefined && articles?.length !== 0 ? (
            <Slider {...settings}>
              {articles?.map((sliderItem, index) => {
                if (index < 5) {
                  return <SliderItem key={uuid()} sliderItem={sliderItem} />;
                }
                return <></>;
              })}
            </Slider>
          ) : (
            <></>
          )}
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

CategoryItem.propTypes = {
  category: PropTypes.string,
};

export default CategoryItem;
