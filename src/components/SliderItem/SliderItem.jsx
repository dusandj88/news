import React from 'react';
import PropTypes from 'prop-types';

const SliderItem = ({ sliderItem }) => (
  <div className="sliderItem__container">
    <p>{sliderItem.title}</p>
    <img src={sliderItem.urlToImage} alt="article" />
  </div>
);

SliderItem.propTypes = {
  sliderItem: PropTypes.object,
};

export default SliderItem;
