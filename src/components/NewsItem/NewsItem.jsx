import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import newsActions from 'redux/news/actions';

const NewsItem = ({ news }) => {
  const newsRedux = useSelector((state) => state.news);
  const dispatch = useDispatch();
  return (
    <div className="news-item__container--without-active_news">
      <p>{news.title}</p>
      <img src={news.urlToImage} alt="news" />
      <p>{newsRedux.active_news === false ? news.description : news.content}</p>
      {newsRedux.active_news === false ? (
        <button
          type="button"
          onClick={() => dispatch(newsActions.setActiveNews(news))}
        >
          More {'>'}
        </button>
      ) : (
        <button
          type="button"
          onClick={() => dispatch(newsActions.setActiveNews(false))}
        >
          {'<'} Back to list
        </button>
      )}
    </div>
  );
};

NewsItem.propTypes = {
  news: PropTypes.object,
};

export default NewsItem;
