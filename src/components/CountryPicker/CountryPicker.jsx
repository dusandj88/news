import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import countryActions from 'redux/country/actions';

const CountryPicker = () => {
  const countryRedux = useSelector((state) => state.country);
  const newsRedux = useSelector((state) => state.news);
  const dispatch = useDispatch();
  return (
    <div
      className={
        newsRedux.active_news === false
          ? 'country-picker__container'
          : 'country-picker__container--disabled'
      }
    >
      <div
        className={
          countryRedux?.country === 'gb'
            ? 'country-picker--active'
            : 'country-picker'
        }
        onClick={() => {
          if (countryRedux?.country !== 'gb') {
            dispatch(countryActions.setCountry('gb'));
          }
        }}
        onKeyPress={() => {
          if (countryRedux?.country !== 'gb') {
            dispatch(countryActions.setCountry('gb'));
          }
        }}
        role="button"
        tabIndex="-1"
      >
        GB
      </div>
      <div
        className={
          countryRedux?.country === 'us'
            ? 'country-picker--active'
            : 'country-picker'
        }
        onClick={() => {
          if (countryRedux.country !== 'us') {
            dispatch(countryActions.setCountry('us'));
          }
        }}
        onKeyPress={() => {
          if (countryRedux.country !== 'us') {
            dispatch(countryActions.country('us'));
          }
        }}
        role="button"
        tabIndex="0"
      >
        US
      </div>
    </div>
  );
};

export default CountryPicker;
