import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import navigationActions from 'redux/navigation/actions';
import newsActions from 'redux/news/actions';

const NavigationItems = () => {
  const navigation = useSelector((state) => state.navigation);
  const dispatch = useDispatch();
  return (
    <div className="navigation-items__container">
      <div
        className={
          navigation?.active_navigation_tab === 0
            ? 'navigation-item--active'
            : 'navigation-item'
        }
        onClick={() => {
          if (navigation?.active_navigation_tab !== 0) {
            dispatch(navigationActions.setActiveNavigationTab(0));
          }
          dispatch(newsActions.setActiveNews(false));
          dispatch(newsActions.setActiveCategory(false));
        }}
        onKeyPress={() => {
          if (navigation?.active_navigation_tab !== 0) {
            dispatch(navigationActions.setActiveNavigationTab(0));
          }
          dispatch(newsActions.setActiveNews(false));
          dispatch(newsActions.setActiveCategory(false));
        }}
        role="button"
        tabIndex="-2"
      >
        Top News
      </div>
      <div
        className={
          navigation?.active_navigation_tab === 1
            ? 'navigation-item--active'
            : 'navigation-item'
        }
        onClick={() => {
          if (navigation?.active_navigation_tab !== 1) {
            dispatch(navigationActions.setActiveNavigationTab(1));
          }
        }}
        onKeyPress={() => {
          if (navigation?.active_navigation_tab !== 1) {
            dispatch(navigationActions.setActiveNavigationTab(1));
          }
        }}
        role="button"
        tabIndex="-1"
      >
        Categories
      </div>
      <div
        className={
          navigation?.active_navigation_tab === 2
            ? 'navigation-item--active'
            : 'navigation-item'
        }
        onClick={() => {
          if (navigation?.active_navigation_tab !== 2) {
            dispatch(navigationActions.setActiveNavigationTab(2));
          }
        }}
        onKeyPress={() => {
          if (navigation?.active_navigation_tab !== 2) {
            dispatch(navigationActions.setActiveNavigationTab(2));
          }
        }}
        role="button"
        tabIndex="0"
      >
        Search
      </div>
    </div>
  );
};

export default NavigationItems;
