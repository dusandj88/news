export const fetchNews = async (country) => {
  const endpoint = `https://newsapi.org/v2/top-headlines?country=${country}&apiKey=821a6fe86fda4e56a42587b88cc246d5`;
  const data = await (await fetch(endpoint)).json();

  if (Object.prototype.hasOwnProperty.call(data, 'message')) {
    return [];
  }

  return data;
};

export const fetchCategoryNews = async (country, category) => {
  const endpoint = `https://newsapi.org/v2/top-headlines?country=${country}&category=${category}&apiKey=821a6fe86fda4e56a42587b88cc246d5`;
  const data = await (await fetch(endpoint)).json();

  if (Object.prototype.hasOwnProperty.call(data, 'message')) {
    return [];
  }

  return data;
};
