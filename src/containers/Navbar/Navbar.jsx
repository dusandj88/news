import React from 'react';
import CountryPicker from 'components/CountryPicker';
import NavigationItems from 'components/NavigationItems';

const Navbar = () => (
  <div className="navbar__container">
    <NavigationItems />
    <CountryPicker />
  </div>
);

export default Navbar;
