import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import NewsItem from 'components/NewsItem';
import { fetchCategoryNews, fetchNews } from 'API';
import Navbar from 'containers/Navbar';
import Categories from 'containers/Categories';

const News = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [news, setNews] = useState([]);

  const reduxCountry = useSelector((state) => state.country);
  const reduxNews = useSelector((state) => state.news);
  const reduxNavigation = useSelector((state) => state.navigation);

  useEffect(() => {
    async function fetchData() {
      let data;
      if (reduxNews.active_category === false) {
        data = await fetchNews(reduxCountry.country);
      } else {
        data = await fetchCategoryNews(
          reduxCountry.country,
          reduxNews.active_category
        );
      }
      if (data !== []) {
        setNews(data.articles);
      }
    }
    fetchData();
  }, [reduxCountry.country, reduxNews.active_category]);

  useEffect(() => {
    const filteredSearchResults = news?.filter((newsItem) =>
      newsItem?.description?.includes(searchTerm)
    );
    setSearchResults(filteredSearchResults);
  }, [searchTerm, news]);

  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };

  return (
    <>
      <Navbar />
      {reduxNavigation.active_navigation_tab === 2 ? (
        <div className="search__container">
          <p>
            Search top news from {reduxCountry.country.toUpperCase()} by term
          </p>
          <input
            type="text"
            placeholder="Search"
            value={searchTerm}
            onChange={handleChange}
          />
        </div>
      ) : (
        <></>
      )}
      {reduxNavigation.active_navigation_tab === 0 ||
      reduxNavigation.active_navigation_tab === 2 ? (
        <div
          className={
            reduxNews.active_news === false ? 'news__container' : 'single-news'
          }
        >
          {reduxNews.active_news === false ? (
            <p className="top-news-from">
              Top{' '}
              {reduxNews.active_category !== false
                ? `${reduxNews.active_category}`
                : ''}{' '}
              news from {reduxCountry.country.toUpperCase()}
            </p>
          ) : (
            <></>
          )}
          {searchResults !== undefined &&
          searchResults?.length !== 0 &&
          reduxNews.active_news === false ? (
            searchResults?.map((newsItem, index) => (
              <NewsItem key={index} news={newsItem} />
            ))
          ) : (
            <></>
          )}
          {reduxNews.active_news !== false ? (
            <NewsItem news={reduxNews.active_news} />
          ) : (
            <></>
          )}
        </div>
      ) : (
        <></>
      )}
      {reduxNavigation.active_navigation_tab === 1 ? <Categories /> : <></>}
    </>
  );
};

export default News;
