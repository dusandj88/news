import React from 'react';
import { useSelector } from 'react-redux';
import CategoryItem from 'components/CategoryItem';
import uuid from 'react-uuid';

const Categories = () => {
  const reduxCountry = useSelector((state) => state.country);
  return (
    <div className="categories__container">
      <p>Top 5 news by categories from {reduxCountry.country.toUpperCase()}</p>
      <div className="categories-items">
        <CategoryItem key={uuid()} category="business" />
        <CategoryItem key={uuid()} category="entertainment" />
        <CategoryItem key={uuid()} category="general" />
        <CategoryItem key={uuid()} category="health" />
        <CategoryItem key={uuid()} category="science" />
        <CategoryItem key={uuid()} category="sports" />
        <CategoryItem key={uuid()} category="technology" />
      </div>
    </div>
  );
};

export default Categories;
