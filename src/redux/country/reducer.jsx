import types from './types';
import initialState from './initialState';
import setCountry from './reducers/setCountry';

const countryReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_COUNTRY:
      return setCountry(state, action);
    default:
      return state;
  }
};

export default countryReducer;
