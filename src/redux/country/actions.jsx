import types from './types';

const setCountry = (country) => ({
  type: types.SET_COUNTRY,
  country,
});

export default {
  setCountry,
};
