import { combineReducers } from 'redux';
import countryReducer from './country/reducer';
import newsReducer from './news/reducer';
import navigationReducer from './navigation/reducer';

const reducer = combineReducers({
  country: countryReducer,
  news: newsReducer,
  navigation: navigationReducer,
});

export default reducer;
