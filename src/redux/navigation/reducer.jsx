import types from './types';
import initialState from './initialState';
import setActiveNavigationTab from './reducers/setActiveNavigationTab';

const navigationReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ACTIVE_NAVIGATION_TAB:
      return setActiveNavigationTab(state, action);

    default:
      return state;
  }
};

export default navigationReducer;
