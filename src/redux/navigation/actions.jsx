import types from './types';

const setActiveNavigationTab = (activeNavigationTab) => ({
  type: types.SET_ACTIVE_NAVIGATION_TAB,
  activeNavigationTab,
});

export default {
  setActiveNavigationTab,
};
