export default (state, action) => ({
  ...state,
  active_navigation_tab: action.activeNavigationTab,
});
