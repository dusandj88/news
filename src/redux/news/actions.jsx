import types from './types';

const setActiveNews = (activeNews) => ({
  type: types.SET_ACTIVE_NEWS,
  activeNews,
});

const setActiveCategory = (activeCategory) => ({
  type: types.SET_ACTIVE_CATEGORY,
  activeCategory,
});

export default {
  setActiveNews,
  setActiveCategory,
};
