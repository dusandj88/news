import types from './types';
import initialState from './initialState';
import setActiveNews from './reducers/setActiveNews';
import setActiveCategory from './reducers/setActiveCategory';

const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_ACTIVE_NEWS:
      return setActiveNews(state, action);
    case types.SET_ACTIVE_CATEGORY:
      return setActiveCategory(state, action);
    default:
      return state;
  }
};

export default newsReducer;
