export default (state, action) => ({
  ...state,
  active_news: action.activeNews,
});
