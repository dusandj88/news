export default (state, action) => ({
  ...state,
  active_category: action.activeCategory,
});
